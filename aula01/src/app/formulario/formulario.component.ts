import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';



@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent  {

  nome = 'Diogenes';
  adicionado = false;
  funcionarios = [];

  id = 0;
  @Output() enviarPessoa = new EventEmitter();

  adicionar(nome: string) {
    this.nome = nome;
    this.adicionado = true;
    
    this.funcionarios.push({
      id: this.id++,
      nome : this.nome
    });

    this.enviarPessoa.emit(this.funcionarios);

  }

  

  }
