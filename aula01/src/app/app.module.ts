import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';


import { AppComponent } from './app.component';
import { FormularioComponent } from './formulario/formulario.component';
import { CartaoPessoaComponent } from './cartao-pessoa/cartao-pessoa.component';
import { TestComponent } from './test/test.component';




@NgModule({
  declarations: [
    AppComponent,
    FormularioComponent,
    CartaoPessoaComponent,
    TestComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
